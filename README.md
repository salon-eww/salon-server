# Salon (server)

Server side code for the Salon application.

## Installation

```bash
yarn

cp .env.sample .env

yarn dev

yarn dev:migrate

yarn seed
```

---

## Libraries Used

-   [Pino - Logger](https://www.npmjs.com/package/pino)
-   [PRISMA - ORM](https://www.npmjs.com/package/prisma)
-   [Express Rate Limiter](https://www.npmjs.com/package/express-rate-limit)

---

## Tech Stack

**Server:** Node, Express, Typescript, MySql

---

## Authors

-   @harrsh
-   @mohit077

## License

[MIT](https://choosealicense.com/licenses/mit/)

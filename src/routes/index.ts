import { Request, Response, Router } from 'express';
import ResponseHandler from '../app/http/middleware/ResponseHandler';
import AdminRouter from './admin';
import AppRouter from './app';

const router = Router();

router.get('/', (req: Request, res: Response) => {
    return ResponseHandler(req, res, {
        message: 'Server is up and running.',
    });
});

router.use('/admin', AdminRouter);
router.use('/app', AppRouter);

export default router;

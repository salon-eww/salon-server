import { Router } from 'express';
import AdminAuthRouter from './auth.route';

const AdminRouter = Router();

AdminRouter.use('/auth', AdminAuthRouter);

export default AdminRouter;

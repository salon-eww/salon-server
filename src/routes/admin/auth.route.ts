import { Router } from 'express';
import AdminSignInController from '../../app/http/admin/controllers/auth/signin.controller';
import { AdminUserSignInRequest } from '../../app/http/admin/requests/auth.request';
import { RequestValidator } from '../../app/http/middleware/RequestValidator';

const AdminAuthRouter = Router();

AdminAuthRouter.post(
    '/signin',
    RequestValidator(AdminUserSignInRequest),
    AdminSignInController
);

export default AdminAuthRouter;

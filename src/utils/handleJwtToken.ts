import jwt from 'jsonwebtoken';
import { env } from '../env';

export const signJWT = (payload: any) => {
    return jwt.sign(payload, env.app.auth.secret);
};

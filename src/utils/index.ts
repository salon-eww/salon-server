// import crypto from 'crypto';

export const validateContactNumber = (number?: string) => {
    const isContactNumberValid = true;
    return isContactNumberValid;
};

export const generateOTP = () => {
    return 1234;
    // return crypto.randomInt(0, 9999);
};

export const pagination = (
    totalCount: number,
    perPage: number,
    page: number
) => {
    return {
        total: totalCount,
        per_page: perPage,
        current_page: page,
        last_page: Math.ceil(totalCount / perPage),
    };
};

import { NextFunction, Request, Response } from 'express';
import _ from 'lodash';
import env from '../../../env';
import { toNumber } from '../../../libs/env';

const PaginationCleaner = async (
    req: Request,
    res: Response,
    next: NextFunction
) => {
    let page = _.get(req, 'query.page', 1);
    let perPage = _.get(req, 'query.per_page', env.app.pagination_limit);
    let sortBy = _.get(req, 'query.sort_by', 'id');
    let sortType = _.get(req, 'query.sort_type', 'asc');

    if (typeof page !== 'string' && typeof page !== 'number') {
        page = 1;
    }

    if (typeof perPage !== 'string' && typeof perPage !== 'number') {
        perPage = env.app.pagination_limit;
    }

    if (typeof sortBy !== 'string') {
        sortBy = 'id';
    }

    if (typeof sortType !== 'string') {
        sortBy = 'asc';
    }

    if (typeof page === 'string') {
        page = toNumber(page);
    }

    if (typeof perPage === 'string') {
        perPage = toNumber(perPage);
    }

    if (page <= 0) {
        page = 1;
    }

    if (perPage <= 0) {
        perPage = env.app.pagination_limit;
    }

    if (!['asc', 'desc'].includes(sortType)) {
        sortType = 'asc';
    }

    req.body.pagination = {
        page: page,
        perPage: perPage,
        sortBy: sortBy,
        sortType: sortType,
    };

    next();
};

export default PaginationCleaner;

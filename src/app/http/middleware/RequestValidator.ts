import { NextFunction, Request, Response } from 'express';
import { AnyObjectSchema } from 'yup';
import ResponseHandler from './ResponseHandler';

export const RequestValidator =
    (resourceSchema: AnyObjectSchema) =>
    async (req: Request, res: Response, next: NextFunction) => {
        try {
            const value = await resourceSchema.validateSync(req.body, {
                abortEarly: false,
                stripUnknown: true,
            });
            req.body.validatedData = value;
            next();
        } catch (error: any) {
            error.message = error.errors[0];
            return ResponseHandler(req, res, null, error);
        }
    };

export const ParamsValidator =
    (resourceSchema: AnyObjectSchema) =>
    async (req: Request, res: Response, next: NextFunction) => {
        try {
            const value = await resourceSchema.validateSync(req.params, {
                abortEarly: false,
                stripUnknown: true,
            });
            req.body.validatedParamsData = value;
            next();
        } catch (error: any) {
            error.message = error.errors[0];
            return ResponseHandler(req, res, null, error);
        }
    };

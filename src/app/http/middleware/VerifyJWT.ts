import jwt, { JwtPayload } from 'jsonwebtoken';
import { NextFunction, Request, Response } from 'express';
import ResponseHandler from './ResponseHandler';
import env from '../../../env';
import { prisma } from '../../providers/db';

export const verifyAdmin = async (
    req: Request,
    res: Response,
    next: NextFunction
) => {
    try {
        const authHeader = req.headers['authorization'];
        const token = authHeader && authHeader.split(' ')[1].trim();

        if (token === null || token === undefined) {
            res.status(401);
            throw new Error('Unauthorized request.');
        }

        const requestUser: string | JwtPayload = jwt.verify(
            token,
            env.app.auth.secret
        );

        if (typeof requestUser === 'string') {
            res.status(400);
            throw new Error('Unauthorized request.');
        }

        const { email, id, role } = requestUser;

        if (role !== 'ADMIN') {
            res.status(401);
            throw new Error('Unauthorized request.');
        }

        const user = await prisma.user.count({
            where: {
                id,
                isDeleted: false,
            },
        });

        if (user < 1) {
            res.status(401);
            throw new Error('Unauthorized request.');
        }

        next();
    } catch (error) {
        return ResponseHandler(req, res, null, error);
    }
};

export const verifyUser = async (
    req: Request,
    res: Response,
    next: NextFunction
) => {
    try {
        const authHeader = req.headers['authorization'];
        const token = authHeader && authHeader.split(' ')[1].trim();

        if (token === null || token === undefined) {
            res.status(401);
            throw new Error('Unauthorized request.');
        }

        const requestUser: string | JwtPayload = jwt.verify(
            token,
            env.app.auth.secret
        );

        if (typeof requestUser === 'string') {
            res.status(401);
            throw new Error('Unauthorized request.');
        }

        const { email, id, role } = requestUser;

        const user = await prisma.user.count({
            where: {
                id,
                isDeleted: false,
            },
        });

        if (user < 1) {
            res.status(401);
            throw new Error('Unauthorized request.');
        }

        next();
    } catch (error) {
        return ResponseHandler(req, res, null, error);
    }
};

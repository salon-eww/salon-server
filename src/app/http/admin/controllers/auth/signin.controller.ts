import { Request, Response } from 'express';
import { generateOTP } from '../../../../../utils';
import { comparePassword } from '../../../../../utils/handlePassword';
import { prisma } from '../../../../providers/db';
import logger from '../../../../providers/logger';
import ResponseHandler from '../../../middleware/ResponseHandler';

/**
 * @url     /admin/auth/signin
 * @method  POST
 * @access  PUBLIC
 */
const AdminSignInController = async (req: Request, res: Response) => {
    try {
        const { email, contactNumber, password } = req.body.validatedData;

        const user = await prisma.user.findFirst({
            where: {
                email,
                contactNumber,
                isDeleted: false,
            },
        });
        if (!user) {
            res.status(400);
            throw new Error('User does not exist.');
        }

        if (!comparePassword(password, user.password)) {
            res.status(400);
            throw new Error('Please enter correct password.');
        }

        const otp = `${generateOTP()}`;
        logger.info(`OTP is ${otp}.`);

        await prisma.user.update({
            where: {
                id: user.id,
            },
            data: {
                otp,
            },
        });

        return ResponseHandler(req, res, {
            message: 'OTP sent successfully.',
        });
    } catch (error) {
        return ResponseHandler(req, res, null, error);
    }
};

export default AdminSignInController;

import { object, string } from 'yup';
import { validateContactNumber } from '../../../../utils';

export const AdminUserSignInRequest = object({
    contactNumber: string()
        .test(
            'validate',
            'Please enter a valid contact number.',
            validateContactNumber
        )
        .required('Contact number is required.'),
    email: string()
        .required('Email is required.')
        .email('Please enter a valid email.'),
    password: string().required('Password is required.'),
});

import { faker } from '@faker-js/faker';
import { Prisma } from '@prisma/client';
import bcrypt from 'bcryptjs';

const USERS: Prisma.userCreateInput[] = [];

const users = async () => {
    USERS.push({
        name: faker.name.fullName(),
        contactNumber: faker.phone.number('##########'),
        email: 'admin@salon.com',
        password: bcrypt.hashSync('1234'),
        role: 'ADMIN',
    });

    USERS.push({
        name: faker.name.fullName(),
        contactNumber: faker.phone.number('##########'),
        email: 'user@salon.com',
        password: bcrypt.hashSync('1234'),
        role: 'USER',
    });
    return USERS;
};

export default users;
